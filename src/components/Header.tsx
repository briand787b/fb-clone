import { FC } from "react";
import { Link } from "react-router-dom";

export const Header: FC = () => {
  return (
      <header className="App-header" style={{display: 'flex', justifyContent: 'space-between'}}>
          <Link to="/">Home</Link>
          <Link to="/friends">Friends</Link>
          <Link to="/marketplace">Marketplace</Link>
          <Link to="/profiles/:id">You</Link>
          <Link to="/login">Log In</Link>
          <Link to="/signup">Sign Up</Link>
      </header>
  );
};
