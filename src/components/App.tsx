import { FC } from "react";
import { Header } from "./Header";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { HomePage } from "../pages/Home";
import { FriendsPage } from "../pages/Friends";
import { ProfilePage } from "../pages/Profile";
import { MarketplacePage } from "../pages/Marketplace";
import { LoginPage } from "../pages/Login";
import { SignupPage } from "../pages/Signup";

export const App: FC = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route path="/friends">
            <FriendsPage />
          </Route>
          <Route path="/profiles/:user_id">
            <ProfilePage />
          </Route>
          <Route path="/marketplace">
            <MarketplacePage />
          </Route>
          <Route path="/login">
            <LoginPage />
          </Route>
          <Route path="/signup">
            <SignupPage />
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};
