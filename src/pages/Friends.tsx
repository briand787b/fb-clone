import {FC} from 'react';

export const FriendsPage: FC = () => {
    return (
        <div>
            <h1>Friends</h1>
            <aside>
                <div>Pending Friend Requests</div>
                <div>Suggested People</div>
            </aside>
            <main>
                <div>Search Friends By Name</div>
            </main>
        </div>
    );
}