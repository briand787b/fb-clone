import {FC} from 'react';

export const MarketplacePage: FC = () => {
    return (
        <div>
            <h1>Marketplace</h1>
            <aside>
                <div>Search Bar</div>
                <div>Categories</div>
            </aside>
            <main>Results</main>
        </div>
    );
}