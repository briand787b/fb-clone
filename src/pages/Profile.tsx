import {FC} from 'react';

export const ProfilePage: FC = () => {
    return (
        <div>
            <h1>Profile</h1>
            <div>hero</div>
            <main>
                <div>Intro</div>
                <div>Photos</div>
                <div>Create Post</div>
                <div>Posts</div>
                <div>Friends</div>
            </main>
        </div>
    );
}