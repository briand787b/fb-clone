import { FC } from "react";

export const HomePage: FC = () => {
  return (
    <div className="home">
      <h1>Home Page</h1>
      <main>
        <div>Stories</div>
        <div>Create Post</div>
        <div>Feed</div>
      </main>
      <aside>Contacts</aside>
    </div>
  );
};
